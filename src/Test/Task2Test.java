package Test;

import com.company.Task2;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class Task2Test {
    private final Task2 cut = new Task2();

    static Arguments[] convertingIntegerNumberToStringTestArgs(){
        return new Arguments[]{
                Arguments.arguments("2", 2),
                Arguments.arguments("Отрицательные числа к натуральным не относят", -2),
        };
    }

    @ParameterizedTest
    @MethodSource("convertingIntegerNumberToStringTestArgs")
    void convertingIntegerNumberToStringTest(String expected, long number) {
        String actual = cut.convertingIntegerNumberToString(number);

        assertEquals(expected, actual);
    }

    static Arguments[] convertingStringToIntegerNumberTestArgs(){
        return new Arguments[]{
                Arguments.arguments(2, "2"),
                Arguments.arguments(-2, "-2"),
        };
    }

    @ParameterizedTest
    @MethodSource("convertingStringToIntegerNumberTestArgs")
    void convertingStringToIntegerNumberTest(long expected, String number) {
        long actual = cut.convertingStringToIntegerNumber(number);

        assertEquals(expected, actual);
    }

    static Arguments[] convertingStringToIntegerNumberTestWithExceptionArgs(){
        return new Arguments[]{
                Arguments.arguments(NumberFormatException.class, "2.0"),
                Arguments.arguments(NumberFormatException.class, "2 2"),
                Arguments.arguments(NumberFormatException.class, "--5"),
                Arguments.arguments(NumberFormatException.class, "++5"),
                Arguments.arguments(NumberFormatException.class, "5l"),
                Arguments.arguments(NumberFormatException.class, ""),
                Arguments.arguments(NumberFormatException.class, null),
        };
    }

    @ParameterizedTest
    @MethodSource("convertingStringToIntegerNumberTestWithExceptionArgs")
    void convertingStringToIntegerNumberTestWithException(Class expected, String number) {
        assertThrows(expected, () -> cut.convertingStringToIntegerNumber(number));
    }

    static Arguments[] convertingStringToRealNumberTestArgs(){
        return new Arguments[]{
                Arguments.arguments(2.2, "2.2"),
                Arguments.arguments(-2.2, "-2.2"),
                Arguments.arguments(2, "2"),
        };
    }

    @ParameterizedTest
    @MethodSource("convertingStringToRealNumberTestArgs")
    void convertingStringToRealNumberTest(double expected, String number) {
        double actual = cut.convertingStringToRealNumber(number);

        assertEquals(expected, actual);
    }

    static Arguments[] convertingStringToRealNumberTestWithExceptionArgs(){
        return new Arguments[]{
                Arguments.arguments(NumberFormatException.class, "2..0"),
                Arguments.arguments(NumberFormatException.class, "2 2"),
                Arguments.arguments(NumberFormatException.class, "--5.0"),
                Arguments.arguments(NumberFormatException.class, "++5.1"),
                Arguments.arguments(NumberFormatException.class, ".3."),
                Arguments.arguments(NumberFormatException.class, ""),
                Arguments.arguments(NullPointerException.class, null),
        };
    }

    @ParameterizedTest
    @MethodSource("convertingStringToRealNumberTestWithExceptionArgs")
    void convertingStringToRealNumberTestWithException(Class expected, String number) {
        assertThrows(expected, () -> cut.convertingStringToRealNumber(number));
    }
}

