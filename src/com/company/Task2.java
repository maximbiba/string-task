package com.company;

public class Task2 {
    public String convertingIntegerNumberToString(long number){
        if(number < 0){
            return "Отрицательные числа к натуральным не относят";
        }
        return String.valueOf(number);
    }

    public String convertingRealNumberToString(double number){
        return String.valueOf(number);
    }

    public long convertingStringToIntegerNumber(String number){
        return Long.parseLong(number);
    }

    public double convertingStringToRealNumber(String number){
        return Double.parseDouble(number);
    }
}

