package com.company;

public class Task1 {
    public String getSymbols(int startInterval, int endInterval){
        String result = "";
        if(startInterval <= endInterval){
            for(int numberOfSymbol = startInterval; numberOfSymbol < endInterval + 1; numberOfSymbol++){
                result = result + " " + (char) numberOfSymbol;
            }
        } else {
            for(int numberOfSymbol = startInterval; numberOfSymbol >= endInterval; numberOfSymbol--) {
                result = result + " " + (char) numberOfSymbol;
            }
        }
        return result;
    }
}
